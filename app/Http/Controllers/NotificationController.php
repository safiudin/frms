<?php

namespace App\Http\Controllers;

use App\Event;
use App\User;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function refresh(){

        //kirim notifikasi buat event besok
        $currentTime = \Carbon\Carbon::now();
        $besok = $currentTime->addDays(1);

        $event = Event::whereDate('date',$besok)->get();
        $user = User::where('is_admin', 0)->get();
        foreach ($event as $key => $value) {
            foreach ($user as $k => $v) {
                $token   = $v->fcm_token;
                $message = $value->name . "Lokasi" . $value->tempat;
                $title   = "Event Besok";
        
                $res = array();
                $res['body']  = $message;
                $res['title'] = $title;
        
                $fields = array(
                    'to' => $token,
                    'notification' => $res,
                );
        
                $url = 'https://fcm.googleapis.com/fcm/send';
                $server_key = "AAAAR8GIkvI:APA91bEgwWRlqxqmaPuCMiX_IuA9ipH4iDhG7xh-VaeNUtNmk6RR5EOEGVzUtjbpa71nq_alXJKbUKF0-Uyd6z1cOMGuJinXeXlOQ--hGcHHcTCpsnq9zQy-zYfaJHBc38pjAbsNxRl9";
        
                $headers = array(
                    'Authorization: key=' . $server_key,
                    'Content-Type: application/json'
                );
                // Open connection
                $ch = curl_init();
        
                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, $url);
        
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
                // Disabling SSL Certificate support temporarly
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        
                // Execute post
                $result = curl_exec($ch);
                if ($result === FALSE) {
                    echo 'Curl failed: ' . curl_error($ch);
                }
        
                // Close connection
                curl_close($ch);
            }
        }


        //kirim notifikasi buat event sekarang
        $currentTime = \Carbon\Carbon::now();

        $event = Event::whereDate('date',$currentTime)->get();
        $user = User::where('is_admin', 0)->get();
        foreach ($event as $key => $value) {
            foreach ($user as $k => $v) {
                $token   = $v->fcm_token;
                $message = $value->name . "Lokasi" . $value->tempat;
                $title   = "Event Hari Ini";
        
                $res = array();
                $res['body']  = $message;
                $res['title'] = $title;
        
                $fields = array(
                    'to' => $token,
                    'notification' => $res,
                );
        
                $url = 'https://fcm.googleapis.com/fcm/send';
                $server_key = "AAAAR8GIkvI:APA91bEgwWRlqxqmaPuCMiX_IuA9ipH4iDhG7xh-VaeNUtNmk6RR5EOEGVzUtjbpa71nq_alXJKbUKF0-Uyd6z1cOMGuJinXeXlOQ--hGcHHcTCpsnq9zQy-zYfaJHBc38pjAbsNxRl9";
        
                $headers = array(
                    'Authorization: key=' . $server_key,
                    'Content-Type: application/json'
                );
                // Open connection
                $ch = curl_init();
        
                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, $url);
        
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
                // Disabling SSL Certificate support temporarly
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        
                // Execute post
                $result = curl_exec($ch);
                if ($result === FALSE) {
                    echo 'Curl failed: ' . curl_error($ch);
                }
        
                // Close connection
                curl_close($ch);
            }
        }


    }

    
}
